# Return Oriented Programming Report

An exercise in code injection, smash stacking, and bypassing executable space protection.

Check the [report](report.pdf) for further details.
Or, if you want to see the source for the latex file and some of the raw notes, check ``src/``.
